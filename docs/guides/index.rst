======
Guides
======

The guides explain all of Typed Settings' features in detail.
You should read them when you know the basics and when you now what exactly you want to build.
If you need inspiration on how to use the different features, you should stop by the :doc:`../../examples` page.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   core
   click
